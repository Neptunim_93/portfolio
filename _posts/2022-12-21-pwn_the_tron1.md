---
layout: default
title: "PWN THE TRON 1"
tags: vulnhub
---

# box [PWN THE TRON 1](https://www.vulnhub.com/entry/pwn-the-tron-1,721/)
<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image1.jpeg" width="100%" height="50%">

10.0.2.1 => host Debian (DHCP)

10.0.2.18 => kali

10.0.2.20 => cible

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image2.jpeg" width="100%" height="50%">

Après un scanne on peut voir qu’il y a un serveur web

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image3.jpeg" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image4.png" width="100%" height="50%">

On a donc une page index.html dans le répertoire /Travel

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image5.jpeg" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image6.jpeg" width="100%" height="50%">

On a donc un hash (MD5) et comment il a été construit, iacon_code fait référence a un utilisateur @deception-base

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image7.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image8.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image9.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image10.png" width="100%" height="50%">

on a donc sur GitHub utilisateur qui a se nom et on peut voir que le ficher est supprimer comme indiquer

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image11.png" width="100%" height="50%">

On peut donc utiliser [https://web.archive.org](https://web.archive.org/) pour récupérer les codes

Il nous faut donc la liste des capitales des pays on peut donc les avois ici [https://geographyfieldwork.com/WorldCapitalCities.htm](https://geographyfieldwork.com/WorldCapitalCities.htm)

on peut trouver le code pour calculer le hash md5 [http://www.zedwood.com/article/cpp-md5-function](http://www.zedwood.com/article/cpp-md5-function)

On peut donc essayer de brut force toutes les combinaisons de chiffre

Le code en C++ :

```cpp
#include <iostream>
#include <fstream>
#include <string>

#include "md5.h"

int main (int argc, char *argv[])
{
  std::ifstream flux("./code.txt");
  std::ifstream flux2("./capital.txt");
  std::string cap, code, data, hash, ref="daab260727e470e56e77ec22e8f3d413";
  while (std::getline(flux, code))
  {
    while (std::getline(flux2, cap))
    {
      std::cout << cap << std::endl;
      for (int i = 0; i < 100; ++i) {
        for (int j = 0; j < 100; ++j) {
          for (int k = 0; k < 100; ++k) {
            for (int l = 0; l < 100; ++l) {
              data = "/" + code + "/" + cap + "/Latitude_" + std::to_string(i) + "."
                    + std::to_string(j) + "-Longitude_" + std::to_string(k) + "." 
                    + std::to_string(l) + ".txt";
              hash = md5(data);
              if (hash == ref) {
                std::cout << data << std::endl;   
              }
            }
          }
        }
      }
    }
  }
  return 0;
}
```

On a donc le chemins

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image13.jpeg" width="100%" height="50%">

Si on se rend à l’emplacement sur le serveur web on a donc un premier flag et on a un chemin ver un shop

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image14.png" width="100%" height="50%">

sur la page du shop on a donc un objet secret a acheté si on essaye de l’acheter on nous demande de nous log

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image15.png" width="100%" height="50%">

On crée un compte on peut voir de que la balance est à zéro.

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image16.png" width="100%" height="50%">

On a une option de transfert

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image17.png" width="100%" height="50%">

Avec un simple test on peut voir comment la requête est effectuer

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image18.png" width="100%" height="50%">

Dans le code source de la page du shop on peut donc voir qu’il y a des uname sur les balises de présentation de trois personnes

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image19.jpeg" width="100%" height="50%">

Le test de remplacer l’origine des font dans le paramètre get. Il y a un formulaire de contact dans la page du shop et si comme contact on met admin_boss et comme message la requête pour transfère des font depuis l’utilisateur lord_starscream

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image20.jpeg" width="100%" height="50%">

[http://10.0.2.20/W4RSHIP_ShoP_transfer.php?to=test&amount=9999&from=lord_starscream](http://10.0.2.20/W4RSHIP_ShoP_transfer.php?to=test&amount=9999&from=lord_starscream)

On a donc désormais les font pour effectuer l’achat

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image21.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image22.png" width="100%" height="50%">

L’achat nous donne un chemin ver une page de login avec un username et un password

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image23.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image24.png" width="100%" height="50%">

Après la connexion on a donc le flag2

Après inspection de la page et du trafic réseaux on peut donc voir la version de PHP exécuter sur le serveur

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image25.png" width="100%" height="50%">

Le serveur est donc vulnérable

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image26.png" width="100%" height="50%">

[https://www.exploit-db.com/exploits/49933](https://www.exploit-db.com/exploits/49933) La version de PHP a été déployer avec une backdoor. On peut donc exécuter une commande en modifier l’User-Agentt (sous la forme ` "User-Agentt": "zerodiumsystem('" + cmd + "'); `)

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image27.png" width="100%" height="50%">

L’exploits sur donner n’offre pas un Shell complait on peut en trouver sur GitHub qui preuve exécuter sur un revsershell

[https://github.com/flast101/php-8.1.0-dev-backdoor-rce/blob/main/revshell_php_8.1.0-dev.py](https://github.com/flast101/php-8.1.0-dev-backdoor-rce/blob/main/revshell_php_8.1.0-dev.py)

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image28.jpeg" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image29.jpeg" width="100%" height="50%">

On peut donc aller dans le home mais on n’a pas les accès pour lier le flag

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image30.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image31.jpeg" width="100%" height="50%">

On peut donc voir que le l’utilisateur Soundware a des clés RSA pour des connexion ssh que l’on peut récupérer

On peut donc se connecter avec sa clé

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image32.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image33.jpeg" width="100%" height="50%">

On peut voir qu’avec un sudo -l l’on peut donc exécuter /usr/bin/vim dans le répertoire /var/Deception/*

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image34.jpeg" width="100%" height="50%">

On peut donc modifier le ficher conf des sudoers

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image35.jpeg" width="100%" height="50%">

Pour pouvoir exécuter en tanque root sans password n’importer qu’elle commande

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image36.png" width="100%" height="50%">

On peut donc passer root

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image37.jpeg" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image38.png" width="100%" height="50%">

<img src="{{ site.baseurl  }}//assets/img/pwn_the_tron1/image39.jpeg" width="100%" height="50%">

Machine rooted
